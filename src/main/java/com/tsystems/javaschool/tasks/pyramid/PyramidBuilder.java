package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers == null)
            throw new CannotBuildPyramidException();

        int n = (int)Math.round(Math.sqrt(2*inputNumbers.size()));

        if(n*(n+1) != 2*inputNumbers.size())
            throw new CannotBuildPyramidException();

        for (Integer el : inputNumbers) {
            if (el == null)
                throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(Comparator.naturalOrder());

        int[][] result = new int[n][2*n-1];
        int k = -1;
        for(int i = 0, j,len, N = n--; i < N; ++i) {
            for (j = 0, len = n - i; j < len; ++j)
                result[i][j] = 0;
            for (j = n-i, len = n+i; j < len; j = j + 2) {
                result[i][j] = inputNumbers.get(++k);
                result[i][j + 1] = 0;
            }
            result[i][n+i] = inputNumbers.get(++k);
            for (j = n+i+1, len = 2*n+1; j < len; ++j)
                result[i][j] = 0;
        }

        return result;
    }


}
