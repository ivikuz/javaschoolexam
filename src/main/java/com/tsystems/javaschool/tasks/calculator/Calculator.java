package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    private String canEvaluate(Stack<String> stack){
        double value2 = Double.parseDouble(stack.pop());
        String op = stack.pop();
        double result = Double.parseDouble(stack.pop());
        switch (op){
            case "*": return Double.toString(result * value2);
            case "/": return Double.toString(result / value2);
            case "+": return Double.toString(result + value2);
            case "-": return Double.toString(result - value2);
            default: return "";
        }
    }
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null)
            return null;

        Character symbol;
        boolean haveDot = false, can1evaluate = false, can2evaluate = false, have1op = false;
        StringBuilder value = new StringBuilder();
        Stack<String> result = new Stack<>();
        for (int i = 0; i < statement.length(); ++i){
            symbol = statement.charAt(i);
            switch (symbol) {
                case '(':
                    result.add(symbol.toString());
                    haveDot=false; can1evaluate = false; can2evaluate = false;
                    break;
                case ')':
                    result.push(value.toString());
                    value.setLength(0);
                    if(can1evaluate) {
                        result.add(canEvaluate(result));
                        can1evaluate = false;
                    }
                    if(can2evaluate) {
                        result.add(canEvaluate(result));
                        can2evaluate = false;
                    }
                    String help_value = result.pop();

                    String el = result.pop();
                    if(!el.equals("("))
                        return null;

                    el = result.pop();
                    if(el.equals("*") || el.equals("/"))
                        can1evaluate = true;
                    result.push(el);
                    result.push(help_value);

                    haveDot=false;
                    break;
                case '/':
                case '*':
                    if(have1op) return null;
                    have1op = true;
                    if(value.length() != 0)
                        result.push(value.toString());
                    if(can1evaluate)
                        result.add(canEvaluate(result));
                    else
                        can1evaluate = true;

                    value.setLength(0);
                    result.add(symbol.toString());
                    haveDot=false;
                    break;
                case '-':
                case '+':
                    if(have1op) return null;
                    have1op = true;
                    if(value.length() != 0)
                        result.push(value.toString());
                    if(can1evaluate) {
                        result.add(canEvaluate(result));
                        can1evaluate = false;
                    }
                    if(can2evaluate)
                        result.add(canEvaluate(result));
                    else
                        can2evaluate = true;

                    value.setLength(0);
                    result.add(symbol.toString());
                    haveDot=false;
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9': have1op = false; value.append(symbol); break;
                case '.': if(haveDot) return null;
                    value.append(symbol);
                    haveDot=true;
                    break;
                case ' ':
                default: return null;
            }
        }

        if(value.length() != 0)
            result.push(value.toString());
        if(can1evaluate)
            result.add(canEvaluate(result));
        if(can2evaluate)
            result.add(canEvaluate(result));



        if(result.size() != 1)
            return null;

        String res = result.pop();
        if(res.equals("Infinity"))
            return null;
        if(Integer.parseInt(res.split("\\.")[1]) == 0)
            return res.split("\\.")[0];
        return res;
    }

}
